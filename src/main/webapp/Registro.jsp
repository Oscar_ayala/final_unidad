<%-- 
    Created on : May 9, 2021, 10:16:19 AM
    Author     : Fabián
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.controller.entity.DiccionarioPalabra"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<DiccionarioPalabra> pala = (List<DiccionarioPalabra>) request.getAttribute("listaPalabras");
    Iterator<DiccionarioPalabra> itPala = pala.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Historial</title>
    </head>
    <body>
        <h1>Historial</h1>
         <form  name="form" action="ConsultaController" method="POST">

       
        <table border="1">
                    <thead>
                    <th>ID Palabra</th>
                    <th>Palabra</th>
                    <th>Significado</th>
                    <th>Fecha</th>
         
                    </thead>
                    <tbody>
                        <%while (itPala.hasNext()) {
                       DiccionarioPalabra cm = itPala.next();%>
                        <tr>
                            <td id="id_palabra"><%= cm.getIdDiccionarioPalabra()%></td>
                            <td id="palabra"><%= cm.getDiccionarioPalabra()%></td>
                            <td id="definicion"><%= cm.getDefinicion()%></td>
                            <td id="fecha"><%= cm.getFecha()%></td>
                         
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                    <br><br>
     
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
        
         </form>   
    </body>
