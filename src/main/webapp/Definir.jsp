<%-- 
    
    Created on : May 9, 2021, 10:17:28 AM
    Author     : Fabián
--%>
<%@page import="com.controller.entity.DiccionarioPalabra"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    DiccionarioPalabra Definir = (DiccionarioPalabra) request.getAttribute("Definir");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definición</title>
    </head>
    <body>
        
        <form  name="form" action="ConsultaController" method="POST">
            <h1><%= Definir.getDiccionarioPalabra().toString() %></h1>
            <h2><%= Definir.getDefinicion().toString() %></h2> 
            <h2>Fuente: Oxford Dictionaries</h2>
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
         </form>
    </body>
</html>
