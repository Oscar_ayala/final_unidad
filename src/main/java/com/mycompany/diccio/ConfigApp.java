package com.mycompany.diccio;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author Fabián
 */
@ApplicationPath("api")
public class ConfigApp extends Application {
    
}
