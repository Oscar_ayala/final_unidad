/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fabián
 */
@Entity
@Table(name = "DiccionarioPalabra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Palabras.findAll", query = "SELECT p FROM DiccionarioPalabra p"),
    @NamedQuery(name = "Palabras.findByIdDiccionarioPalabra", query = "SELECT p FROM DiccionarioPalabra p WHERE p.idDiccionarioPalabra = :idDiccionarioPalabra"),
    @NamedQuery(name = "Palabras.findByDiccionarioPalabra", query = "SELECT p FROM DiccionarioPalabra p WHERE p.DiccionarioPalabra = :DiccionarioPalabra"),
    @NamedQuery(name = "Palabras.findByDefinicion", query = "SELECT p FROM DiccionarioPalabra p WHERE p.definicion = :definicion"),
    @NamedQuery(name = "Palabras.findByFecha", query = "SELECT p FROM DiccionarioPalabra p WHERE p.fecha = :fecha")})
public class DiccionarioPalabra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "id_DiccionarioPalabra")
    private String idDiccionarioPalabra;
    @Size(max = 2147483647)
    @Column(name = "DiccionarioPalabra")
    private String DiccionarioPalabra;
    @Size(max = 2147483647)
    @Column(name = "definicion")
    private String definicion;
    @Size(max = 2147483647)
    @Column(name = "fecha")
    private String fecha;

    public DiccionarioPalabra() {
    }

    public DiccionarioPalabra(String idDiccionarioPalabra) {
        this.idDiccionarioPalabra = idDiccionarioPalabra;
    }

    public String getIdDiccionarioPalabra() {
        return idDiccionarioPalabra;
    }

    public void setIdDiccionarioPalabra(String idPalabra) {
        this.idDiccionarioPalabra = idPalabra;
    }

    public String getDiccionarioPalabra() {
        return DiccionarioPalabra;
    }

    public void setDiccionarioPalabra(String DiccionarioPalabra) {
        this.DiccionarioPalabra = DiccionarioPalabra;
    }

    public String getDefinicion() {
        return definicion;
    }

    public void setDefinicion(String definicion) {
        this.definicion = definicion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDiccionarioPalabra != null ? idDiccionarioPalabra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiccionarioPalabra)) {
            return false;
        }
        DiccionarioPalabra other = (DiccionarioPalabra) object;
        if ((this.DiccionarioPalabra == null && other.idDiccionarioPalabra != null) || (this.idDiccionarioPalabra != null && !this.idDiccionarioPalabra.equals(other.idDiccionarioPalabra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.controller.entity.DiccionarioPalabra[ idDiccionarioPalabra=" + idDiccionarioPalabra + " ]";
    }
    
}
